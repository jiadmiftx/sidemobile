import 'package:flutter/material.dart';
import 'dart:async';
import 'login.dart';
import 'home.dart';

class Splash extends StatefulWidget {
  static String tag = 'splash_screen';
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(
        Duration(seconds: 4), () => Navigator.of(context).pushNamed(Login.tag));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal[300],
      body: Center(
          child: ListView(
              padding: EdgeInsets.only(left: 24.0, right: 24.0),
              children: <Widget>[
            new Padding(
              padding: new EdgeInsets.only(top: 90.0),
              child: new Image(
                width: 130.0,
                height: 100.0,
                fit: BoxFit.scaleDown,
                image: AssetImage('assets/village.png'),
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            new Padding(
              padding: new EdgeInsets.only(top: 2.0),
              child: new Image(
                width: 100.0,
                height: 70.0,
                fit: BoxFit.scaleDown,
                image: AssetImage('assets/logoside.png'),
              ),
            ),
          ])),
    );
  }
}
