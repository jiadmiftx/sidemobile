import 'package:flutter/material.dart';
import 'destinations/map_screen.dart';
import 'destinations/form_penduduk.dart';
import 'destinations/info_desa.dart';
import 'destinations/form_pembangunan.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class HomeActivity extends StatefulWidget {
  static String tag = 'home_activity';

  @override
  _HomeActivityState createState() => _HomeActivityState();
}

class _HomeActivityState extends State<HomeActivity> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[100],
      body: ListView(
        children: <Widget>[
          Material(
            elevation: 3,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 175.0,
                  width: double.infinity,
                  color: Colors.yellow[600],
                ),
                Positioned(
                  bottom: 30.0,
                  left: 320.0,
                  child: Container(
                    height: 30,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200.0),
                      color: Colors.white24,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 10.0,
                  left: 170.0,
                  child: Container(
                    height: 30,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200.0),
                      color: Colors.white24,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 90.0,
                  left: 130.0,
                  child: Container(
                    height: 30,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200.0),
                      color: Colors.white24,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 40.0,
                  left: 30.0,
                  child: Container(
                    height: 30,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200.0),
                      color: Colors.white24,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 150.0,
                  right: 300.0,
                  child: Container(
                    height: 30,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200.0),
                      color: Colors.white24,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 150.0,
                  right: 10.0,
                  child: Container(
                    height: 30,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200.0),
                      color: Colors.white24,
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              height: 50.0,
                              width: 50.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25.0),
                                border: Border.all(
                                    color: Colors.white,
                                    style: BorderStyle.solid,
                                    width: 2.0),
                                image: DecorationImage(
                                  image: AssetImage('assets/penduduk.jpeg'),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                  "Miftahul Jihad",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Exo',
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "Admin",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Exo',
                                      fontStyle: FontStyle.italic),
                                )
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Container(
                          alignment: Alignment.topRight,
                          child: IconButton(
                            icon: Icon(Icons.menu),
                            onPressed: () {},
                            color: Colors.white,
                            iconSize: 30.0,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 55.0),
                    Padding(
                      padding: EdgeInsets.only(left: 15.0, right: 15.0),
                      child: Material(
                        elevation: 3.0,
                        borderRadius: BorderRadius.circular(5.0),
                        child: TextFormField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(Icons.search,
                                color: Colors.black, size: 30.0),
                            contentPadding:
                                EdgeInsets.only(left: 15.0, top: 15.0),
                            hintText: 'Cari Data',
                            hintStyle: TextStyle(
                                color: Colors.grey, fontFamily: 'Exo'),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Stack(
            children: <Widget>[
              SizedBox(height: 10.0),
              Material(
                  elevation: 3.0,
                  child: Container(height: 75.0, color: Colors.white)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: 75.0,
                    width: MediaQuery.of(context).size.width / 4,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/gis.png'))),
                        ),
                        Text(
                          'Maps',
                          style: TextStyle(fontFamily: 'Exo'),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 75.0,
                    width: MediaQuery.of(context).size.width / 4,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/031-teamwork.png'))),
                        ),
                        Text(
                          'People',
                          style: TextStyle(fontFamily: 'Exo'),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 75.0,
                    width: MediaQuery.of(context).size.width / 4,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/village.png'))),
                        ),
                        Text(
                          'Village',
                          style: TextStyle(fontFamily: 'Exo'),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 75.0,
                    width: MediaQuery.of(context).size.width / 4,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/039-info.png'))),
                        ),
                        Text(
                          'Info',
                          style: TextStyle(fontFamily: 'Exo'),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 8, top: 8, right: 4, bottom: 4),
                width: MediaQuery.of(context).size.width / 2,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(FormPenduduk.tag);
                  },
                  child: Material(
                    elevation: 3,
                    borderRadius: BorderRadius.circular(20),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(40.0, 40.0, 40.0, 0),
                          child: Image(
                            image: AssetImage('assets/031-teamwork.png'),
                          ),
                        ),
                        Text(
                          "Penduduk",
                          style: TextStyle(
                            color: Colors.yellow[700],
                            fontSize: 24,
                            fontFamily: 'Exo',
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 4, top: 8, right: 8, bottom: 4),
                width: MediaQuery.of(context).size.width / 2,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(MapScreen.tag);
                  },
                  child: Material(
                    elevation: 3,
                    borderRadius: BorderRadius.circular(20),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(40.0, 40.0, 40.0, 0),
                          child: Image(
                            image: AssetImage('assets/005-placeholder.png'),
                          ),
                        ),
                        Text(
                          "Gis Desa",
                          style: TextStyle(
                            color: Colors.yellow[700],
                            fontSize: 24,
                            fontFamily: 'Exo',
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 8, top: 8, right: 4, bottom: 4),
                width: MediaQuery.of(context).size.width / 2,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(FormPembangunan.tag);
                  },
                  child: Material(
                    elevation: 3,
                    borderRadius: BorderRadius.circular(20),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(40.0, 40.0, 40.0, 20.0),
                          child: Image(
                            image: AssetImage('assets/village.png'),
                          ),
                        ),
                        Text(
                          "Pembangunan",
                          style: TextStyle(
                            color: Colors.yellow[700],
                            fontSize: 24,
                            fontFamily: 'Exo',
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 4, top: 8, right: 8, bottom: 4),
                width: MediaQuery.of(context).size.width / 2,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(Infodesa.tag);
                  },
                  child: Material(
                    elevation: 3,
                    borderRadius: BorderRadius.circular(20),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(40.0, 40.0, 40.0, 0),
                          child: Image(
                            image: AssetImage('assets/039-info.png'),
                          ),
                        ),
                        Text(
                          "Info Desa",
                          style: TextStyle(
                            color: Colors.yellow[700],
                            fontSize: 24,
                            fontFamily: 'Exo',
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

// List<StaggeredTile> _staggeredTiles = const <StaggeredTile>[
//   const StaggeredTile.count(4, 2),
//   const StaggeredTile.count(2, 2),
//   const StaggeredTile.count(2, 4),
//   const StaggeredTile.count(2, 2)
// ];

// List<Widget> _tiles = const <Widget>[
//   const Homeactivity(Color(0xFF4DB6AC), AssetImage('assets/wallpaperdesa.jpg'),
//       AssetImage('assets/sidesa.png'), MapScreen.tag),
//   const Homeactivity(Color(0xFF4DB6AC), AssetImage('assets/wallpaperdesa.jpg'),
//       AssetImage('assets/desa.jpg'), FormPenduduk.tag),
//   const Homeactivity(Color(0xFF4DB6AC), AssetImage('assets/wallpaperdesa.jpg'),
//       AssetImage('assets/desa.jpg'), Infodesa.tag),
//   const Homeactivity(Color(0xFF4DB6AC), AssetImage('assets/wallpaperdesa.jpg'),
//       AssetImage('assets/desa.jpg'), FormPembangunan.tag),
// ];

// class HomePage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: new AppBar(
//           title: new Text("SIDE"),
//         ),
//         body:
// new Padding(
//     padding: const EdgeInsets.only(top: 12.0),
//     child: new StaggeredGridView.count(
//       crossAxisCount: 4,
//       staggeredTiles: _staggeredTiles,
//       children: _tiles,
//       mainAxisSpacing: 7.0,
//       crossAxisSpacing: 7.0,
//       padding: const EdgeInsets.all(7.0),
//     )));
//   }
// }

// class Homeactivity extends StatelessWidget {
//   static String tag = 'home_activity';

//   @override
//   State<StatefulWidget> createState() {
//     // TODO: implement createState
//   }

//   // const Homeactivity(
//   //     this.backgroundColor, this.backgImage, this.assetImage, this.destination);

//   // final Color backgroundColor;
//   // final AssetImage assetImage;
//   // final AssetImage backgImage;
//   // final String destination;

//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return new Container(
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(30)
//       ),
//       margin: EdgeInsets.all(2.0),
//       child: new Material(
//         borderRadius: BorderRadius.circular(30),
//         child: new InkWell(
//           borderRadius: BorderRadius.circular(30),
//           onTap: () {
//             Navigator.of(context).pushNamed(destination);
//           },
//           child: new Stack(
//             fit: StackFit.expand,
//             children: <Widget>[
//               Image(
//                 fit: BoxFit.cover,
//                 image: assetImage,
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
