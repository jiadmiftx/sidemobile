import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'model.dart';

void showResultsKpnddukan(BuildContext context, FormModelPenduduk model) {
  showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Update Result'),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildResultsRow('Nama', model.nama),
                _buildResultsRow('Tempat Lahir', model.tempat_lahir),
                _buildResultsRow(
                    'Tgl. Lahir', DateFormat.yMd().format(model.tgl_lahir)),
                _buildResultsRow('Alamat', model.alamat),
                _buildResultsRow('Jenis Kelamin', model.jk),
                _buildResultsRow('Agama', model.agama),
                _buildResultsRow('Pend. Terakhir', model.pend_terakhir),
                _buildResultsRow('Pekerjaan', model.pekerjaan),
                _buildResultsRow('Status', model.status),
                _buildResultsRow('Status Keluarga', model.stts_keluarga),
                _buildResultsRow('Gol. Darah', model.gol_darah),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      });
}

void showResultsPmbngn(BuildContext context, FormModelPembangunan model) {
  showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Update Result'),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildResultsRow('Nama Desa', model.desa),
                // _buildResultsRow('Tahun', model.tahun),
                // _buildResultsRow(
                //     'Tgl. Lahir', DateFormat.yMd().format(model.tgl_lahir)),
                // _buildResultsRow('Alamat', model.alamat),
                // _buildResultsRow('Jenis Kelamin', model.jk),
                // _buildResultsRow('Agama', model.agama),
                // _buildResultsRow('Pend. Terakhir', model.pend_terakhir),
                // _buildResultsRow('Pekerjaan', model.pekerjaan),
                // _buildResultsRow('Status', model.status),
                // _buildResultsRow('Status Keluarga', model.stts_keluarga),
                // _buildResultsRow('Gol. Darah', model.gol_darah),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      });
}

Widget _buildResultsRow(String nama, dynamic value, {bool linebreak: false}) {
  return Column(
    children: <Widget>[
      Row(
        children: <Widget>[
          Expanded(
            child:
                Text('$nama:', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          _buildValueInLine(value, linebreak)
        ],
      ),
      _buildValueOnOwnRow(value, linebreak),
      Container(
        height: 12.0,
      )
    ],
  );
}



Widget _buildValueInLine(dynamic value, bool linebreak) {
  return (linebreak) ? Container() : Text(value.toString());
}

Widget _buildValueOnOwnRow(dynamic value, bool linebreak) {
  return (linebreak) ? Text(value.toString()) : Container();
}


