// import 'package:map_view/figure_joint_type.dart';
// import 'package:map_view/polygon.dart';
import 'package:flutter/material.dart';
// import 'package:map_view/map_view.dart';


class MapScreen extends StatefulWidget {
static const String tag = 'gis_activity';
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
//   MapView mapView = new MapView();

//   void showMap() {
//     mapView.show(new MapOptions(showUserLocation: true));
// }
//   List<Marker> markers = <Marker> [
//     new Marker("1", "Lumbung Inovasi", -8.581656, 116.101388, color: Colors.teal, draggable: true)
//   ];

//   List<Polygon> polygons = <Polygon>[
//     new Polygon("One", <Location>[
//       new Location(-8.581206, 116.101360),
//       new Location(-8.581256, 116.101759),
//       new Location(-8.582317, 116.101626),
//       new Location(-8.582182, 116.101130),
//     ],
//     jointType: FigureJointType.round,
//     strokeColor: Colors.blue,
//     strokeWidth: 10.0,
//     fillColor: Colors.teal.withOpacity(0.1)
//     )
//   ];

//   displayMap(){
//     mapView.show(new MapOptions(

//       mapViewType: MapViewType.normal,
//       initialCameraPosition: new CameraPosition(new Location(-8.581656, 116.101388), 15.0),
//       showUserLocation: false,
//       title: 'GIS Desa'
//     ));

//   mapView.onMapTapped.listen((tapped){
//     mapView.setMarkers(markers);
//     mapView.setPolygons(polygons);
//     mapView.zoomToFit(padding: 100);
//   });
//   }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('GIS'),
      ),
      body: new Center(
        child: Container(
          child: RaisedButton(
            child: Text('Tap me'),
            color: Colors.teal,
            textColor: Colors.white,
            elevation: 7.0,
            onPressed: (){},
          ),
        ),
      ),
    );
  }
}
